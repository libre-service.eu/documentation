# Journal 2021

# 2021-10-30

* installation de la vm libre-service.eu
* déploiement de Mumble pour le service audio.libre-service.eu
* déploiement de PrivateBin pour le service paste.libre-service.eu
* déploiement de LibreQR pour le service qrcode.libre-service.eu
* installation de la vm visio.libre-service.eu
* déploiement de Jitsi pour le service visio.libre-service.eu

# 2021-10-31

* dépôt candidature au collectif CHATONS

