# Documentation de Libre-service.eu

Bienvenue sur la documentation de « Libre-service.eu ».

L'ensemble de la documention est sous licence CC-BY-SA+ (https://creativecommons.org/licenses/by-sa/4.0/).

# L'infrastructure
* <a href="./documentation/src/branch/master/infra/architecture.md">architecture</a>
* <a href="./documentation/src/branch/master/infra/letsencrypt.md">LetsEncrypt</a>
* paquets Debian utilisés sur les serveurs : <a href="./documentation/src/branch/master/infra/packages-2021.list">2021</a>, <a href="./documentation/src/branch/master/infra/packages-2022.list">2022</a>, <a href="./documentation/src/branch/master/infra/packages-2023.list">2023</a>
* rapports vrms : <a href="./documentation/src/branch/master/infra/vrms-2022.list">2022</a>, <a href="./documentation/src/branch/master/infra/vrms-2023.list">2023</a>
* <a href="./documentation/src/branch/master/infra/ssh.md">configuration SSH</a>
* <a href="./documentation/src/branch/master/infra/apache.md">configuration serveur Apache</a>
* <a href="./documentation/src/branch/master/infra/tomcat.md">configuration Tomcat</a>

# Les services

Les services d'infrastructure :
* <a href="./documentation/src/branch/master/infra/www.libre-service.eu.md">www.libre-service.eu</a>
* <a href="./documentation/src/branch/master/infra/infos.libre-service.eu.md">infos.libre-service.eu</a>

Les services publics :
* <a href="./documentation/src/branch/master/services/audio.libre-service.eu.md">audio.libre-service.eu</a>
* <a href="./documentation/src/branch/master/services/minetest.libre-service.eu.md">minetest.libre-service.eu</a>
* <a href="./documentation/src/branch/master/services/pad.libre-service.eu.md">pad.libre-service.eu</a>
* <a href="./documentation/src/branch/master/services/paste.libre-service.eu.md">paste.libre-service.eu</a>
* <a href="./documentation/src/branch/master/services/qrcode.libre-service.eu.md">qrcode.libre-service.eu</a>
* <a href="./documentation/src/branch/master/services/visio.libre-service.eu.md">visio.libre-service.eu</a>

Les services candidats :
* Etherpad : bloc note collaboratif ;
* Gitea, forge logicielle ;
* Juga : conjugaison des verbres en français ;
* Mastodon : microblogging ;
* Nextcloud : partage et synchronisation de fichiers, agenda, carnet d'adresses…
* Trivabble : jeu de Scrabble en réseau ;
* …