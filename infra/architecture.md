# Architecture

### Machine physique principale
* hébergeur : Hetzner Robot
* contrat : location serveur dédié
* localisation : FSN1-DC20, Data Center Park Falkenstein, Allemagne
* système d'exploitation : Debian stable (12 bookworm)
* processeur : Intel Xeon E3-1275 v6, 3.80GHz, 14nm, 4c/8t, PDT 73W, Q1'17
* mémoire : 64 Go
* disques : 2 x 4 To
* hyperviseur : KVM/qemu

Jusqu'en novembre 2023 :
* hébergeur : SoYouStart (OVH)
* contrat : location serveur dédié
* localisation : RBX1 (Roubaix, France)
* système d'exploitation : Debian stable (11 bullseye)
* processeur : Intel Xeon E3-1245 v2, 3.40GHz, 22nm, 4c/8t, PDT 77W, Q2'12
* mémoire : 32 Go
* disques : 2 x 2 To
* hyperviseur : KVM/qemu

### Machines virtuelles
* système d'exploitation : Debian stable (12 bookworm)

### Sauvegardes
* logiciel : Borgbackup + Borgmatic
* localisation : Massy (91300), France
* chiffrement : activé
* connexion : fibre optique
* fréquence : quotidienne
* rétention : 6 mois maximum

### Certificats SSL
* fournisseur : Let's Encrypt
* logiciel : dehydrated

### Gestion DNS
* hébergeur nom de domaine : Gandi
* serveur DNS des vm : OVH

### Routage
* IPv4 :
  * jusqu'en novembre 2023 : macvtap d'un bloc d'ip,
  * depuis novembre 2023 : proxy ipv4
* IPv6 : depuis novembre 2023

### Firewall
* avant novembre 2023 : scripts iptables sur chaque vm
* après novembre 2023 : ufw

### Serveur de courriel
* avant novembre 2023 : fournisseur : Gandi
* après novembre 2023 : Bookmyname

### Divers
* serveur web Apache
* service Tomcat 9
* EtcKeeper
* vrms
* rétention des logs : 3 mois