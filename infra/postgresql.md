# Postgresql

Installer Postgresql :
```
apt-get install postgresql postgresql-contrib
```

Pour permettre des actions via `psql`, mettre un mot de passe au compte `postgresql` :
```
password postgresql
```
