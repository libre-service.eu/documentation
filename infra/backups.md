# Backups

Borgmatic is used to manage backups.


```
apt-get install borgmatic
```

Generate default Borgmatic configuration:
```
generate-borgmatic-config
```

Create a file containing excludes `/etc/borgmatic/excludes`:
```
pp:usr/bin/
pp:usr/lib/
pp:usr/sbin/
pp:usr/share/
pp:var/cache/
fm:*/.gvfs
fm:*/.thumbnails
fm:*/.bkl-thumbnails
fm:*/Cache
fm:*/.cache
fm:*/cache
fm:*/cache2
fm:*/.davfs2
fm:*/.local/share/Trash
fm:*/VCache
fm:*/lost+found
fm:*/Sauvegardes
fm:*/kvm/images/*
fm:*/kvm/ISO/*
```
Documentation about pattern: https://borgbackup.readthedocs.io/en/stable/usage/help.html?highlight=pattern

Modify the Borgmatic configuration `/etc/borgmatic/config.yaml`:
```
location:
    source_directories:
        - /etc
        - /home
        - /opt
        - /root
        - /srv
        - /usr
        - /var

    repositories:
        - backuplogin@backupmachine:/path/to/backup/Borg.repo

    exclude_from:
        - /etc/borgmatic/excludes

storage:
     ssh_command: ssh -p XXXXX
     # archive_name_format: '{fqdn}-{now:%Y-%m-%dT%H:%M}'
     archive_name_format: '{fqdn}-{now:%Y%m%d}'

retention:
     prefix: '{fqdn}-'

output:
    color: false

```

Check the Borgmatic configuration file:
```
validate-borgmatic-config
```

Define the borgbackup machine in `/etc/hosts`:
```
xxx.xxx.xxx.xxx backupmachine
```

Create local SSH keys:
```
ssh-keygen -o -a 100 -t rsa -b 5120 -C "root@fooland.fr"
```


## SSh timeout

Sometimes, there is broken pipe with ssh. Add the following to the `~/.ssh/config` file on the client:
```
 Host *
        ServerAliveInterval 120
```
