# Apache

## Configuration du logrotate

Convention d'un dossier de log par site web donc nécessité de personnaliser le logrotate :  
```
/var/log/apache2/*.log
/var/log/apache2/*/*log
{
    monthly
    missingok
    rotate 6
…
```

## Prérequis

Activer le module `headers` :
```
a2enmode headers
apachectl configtest
systemctl reload Apache2
```

## Activation de HSTS

Créer le fichier `/etc/apache2/hsts.conf` :
```
# HTTP Strict Transport Security (mod_headers is required) (63072000 seconds)                                     
Header always set Strict-Transport-Security "max-age=63072000"
``` 

Inclure la configuration HSTS dans la configuration des sites web hébergés :
``` 
<VirtualHost *:443>
        […]
        Include hsts.conf
        […]
</VirtualHost>
```

## Activation de CSP

Créer le fichier `/etc/apache2/csp.conf` :
```
Header set Content-Security-Policy "default-src 'self'; frame-ancestors 'self';"
Header set X-Frame-Options "SAMEORIGIN"
``` 

Inclure la configuration HSTS dans la configuration des sites web hébergés :
``` 
<VirtualHost *:443>
        […]
        Include csp.conf
        […]
</VirtualHost>
```

## Vérification Mozilla Observatory

Mozilla met à disposition un service pour évaluer la configuration d'un site web. Vive recommandation de l'utiliser.

Exemples :
* https://observatory.mozilla.org/analyze/www.libre-service.eu
