# Bash

# Customize prompt:

Avant : 
```
root@libre-service:~#
```

Modifier le nom de la machine :
```
hostnamectl set-hostname libre-service.eu
```


Modifier /root/.bahsrc :
```
PS1='${debian_chroot:+($debian_chroot)}\u@\H:\w\$ '
```

Après : 
```
root@libre-service.eu:~#
```
