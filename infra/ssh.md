# SSH

## Sécurité
Précaution élémentaire dans `/etc/ssh/sshd_config` :
```
PermitRootLogin prohibit-password
PasswordAuthentication no
```

## Environnement

Très pratique d'avoir des variables d'environnement correctement configurées pour GIT. 

Côté serveur, éditer le fichier `/etc/ssh/sshd_config` :
```
-AcceptEnv LANG LC_*
+AcceptEnv LANG LC_* GIT_*
```

Côté client, éditer le fichier `~/.ssh/config` :
```
# Libre-service.eu
Host libre-service.eu
    User root
    IdentityFile ~/.ssh/foo.pub
    ForwardAgent no
    SetEnv GIT_AUTHOR_NAME="Christian P. MOMON" GIT_AUTHOR_EMAIL="christian@devinsy.fr"
    SendEnv GIT_AUTHOR_NAME GIT_AUTHOR_EMAIL
```
