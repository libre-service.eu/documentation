# Service infos.libre-service.eu

StatoolInfos est un outil et un protocole pour partager des informations sur une fédération de services.

# Déploiement

## Configuration d'Apache

Création d'un fichier générique `cat /etc/apache2/statoolinfos.conf` qui sera includé plus tard :
```
    Alias "/.well-known/statoolinfos/" "/srv/statoolinfos/well-known/statoolinfos/"
    <Directory "/srv/statoolinfos/well-known/statoolinfos/">
        Options +Indexes
        Require all granted
    </Directory>
```

## Configuration de Nginx
Création d'un fichier générique `cat /etc/nginx/statoolinfos.conf` qui sera includé plus tard :
```
   location /.well-known/statoolinfos/ 
   {
       charset utf-8;
       types 
       {
            text/plain    properties;
       }

       alias /srv/statoolinfos/well-known/statoolinfos/;
       autoindex on;
   }
```
## Espace de travail
Dossiers de travail :
```
mkdir -p /srv/statoolinfos/{conf,inputs,well-known}
```

# Ajout d'un nouveau service

Tout se passe sur la vm du service, sauf la dernière étape.

## Configuration d'Apache

Création d'un fichier générique `cat /etc/apache2/statoolinfos.conf` qui sera includé plus tard :
```
    Alias "/.well-known/statoolinfos/" "/srv/statoolinfos/well-known/statoolinfos/"
    <Directory "/srv/statoolinfos/well-known/statoolinfos/">
        Options +Indexes
        Require all granted
    </Directory>
```

## Configuration de Nginx
Création d'un fichier générique `cat /etc/nginx/statoolinfos.conf` qui sera includé plus tard :
```
   location /.well-known/statoolinfos/ 
   {
       charset utf-8;
       types 
       {
            text/plain    properties;
       }

       alias /srv/statoolinfos/well-known/statoolinfos/;
       autoindex on;
   }
```

## Espace de travail
Dossiers de travail :
```
mkdir -p /srv/statoolinfos/{conf,well-known}
mkdir -p /srv/statoolinfos/well-known/statoolinfos/
```

## Déclaration du service
Configurer l'accès well-known en ajoutant à la fin de la configuration Apache du site :
```
    # StatoolInfos
    Include statoolinfos.conf
</VirtualHost>
```
Ou au début du fichier de la configuration Nginx du site :
```
    # StatoolInfos.
    include /etc/nginx/statoolinfos.conf;
```

Créer le fichier properties de type service à `/srv/statoolinfos/well-known/statoolinfos/foo.libre-service.eu.properties`. Une documentation des propriétés est disponible là : https://framagit.org/chatons/chatonsinfos/-/blob/master/MODELES/service.properties

Vérifier les permissions de lecture :
```
chmod go+r /srv/statoolinfos/well-known/statoolinfos/*
```

## Renseigner le fichier properties d'organisation

Sur la vm principale de StatoolInfos, éditer `/srv/statoolinfos/well-known/statoolinfos/services.properties` et ajouter une ligne `subs` :
```
subs.foo=https://foo.libre-service.eu/.well-known/statoolinfos/foo.libre-service.eu.properties
```

# Génération de métrics

## Configuration de Java
Java :
```
apt-get install openjdk-11-jre-headless
```

## Déploiement de statoolinfos

Récupérer la dernière version de StatoolInfos sur https://forge.devinsy.fr/devinsy/statoolinfos/releases. Puis placer dans `/srv/statoolinfos/bin/` les fichiers `statoolinfos.jar` et `statoolinfos.sh`. Faire un lien :
```
cd /srv/statoolinfos/bin/
ln -s statoolinfos.sh statoolinfos
```

## Configuration
Créer un fichier de configuration dans `/srv/statoolinfos/conf/foo.libre-service.eu.conf` :
```
conf.class=service
conf.protocol=StatoolInfos-0.5

conf.probe.types=HttpAccessLog, HttpErrorLog
conf.probe.httpaccesslog.file=/var/log/nginx/foo.libre-service.eu-access.log*
conf.probe.httperrorlog.file=/var/log/nginx/foo.libre-service.eu-error.log*
conf.probe.target=/srv/statoolinfos/well-known/statoolinfos/foo.libre-service.eu-metrics.properties
```

Vérifier les permissions de lecture :
```
chmod go+r /srv/statoolinfos/well-known/statoolinfos/*
```

## Génération

Faire une première génération de métrics :
```
/srv/statoolinfos/bin/statoolinfos probe -full /srv/statoolinfos/conf/
```
Vérifier les permissions de lecture :
```
chmod go+r /srv/statoolinfos/well-known/statoolinfos/*
```

Créer un cron `/etc/cron.d/staoolinfos` :
```
2 * * * * root /srv/statoolinfos/bin/statoolinfos probe -previousday /srv/statoolinfos/conf/ >> /srv/statoolinfos/statoolinfos-cron.log
```

Faire un lien :
```
cd /srv/statoolinfos
ln -s /etc/cron.d/statoolinfos cron
```
