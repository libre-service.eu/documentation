# Let's encrypt

# Installation
Installer les paquets :
```
apt-get install dehydrated dehydrated-apache2
```

Recharger Apache :
```
systemctl reload apache2.service
```

S'enregistrer :
```
/usr/bin/dehydrated --register --accept-terms
```

# Générer un certificat

Ouvrir les ports http (80) et https (443) :
```
ufw allow '80'
ufw allow '443'
```

Configurer a minima le site web dans `/etc/apache2/sites-available/foo.libre-service.org.conf` :
```
<VirtualHost *:80>
        ServerName foo.libre-service.eu
        ServerAdmin admin@libre-service.eu

        CustomLog ${APACHE_LOG_DIR}/foo.libre-service.eu/infos.libre-service.eu-access.log combined
        ErrorLog  ${APACHE_LOG_DIR}/foo.libre-service.eu/infos.libre-service.eu-error.log
        LogLevel warn

        Redirect 302 / https://foo.libre-service.eu/
</VirtualHost>
```

Activer la configuration :
```
a2ensite foo.libre-service.eu.conf
```

Vérifier que c'est bon et recharger :
```
apachectl configtest && systemctl reload apache2
```

Vérifier le logrotate des logs :
```
/var/log/apache2/*.log
/var/log/apache2/*/*log
{
    monthly
    missingok
    rotate 6
…
```

Ajouter foo.libre-service.eu dans `/etc/dehydrated/domains.txt`.

Lancer la génération :
```
/usr/bin/dehydrated -c
```

Le certificat est généré dans `/var/lib/dehydrated/certs/foo.libre-service.eu/`.

Compléter la partie SSL  `/etc/apache2/sites-enabled/foo.libre-service.org.conf` :
``` 
<VirtualHost *:443>
        ServerName pad.libre-service.eu
        ServerAdmin admin@libre-service.eu

        CustomLog ${APACHE_LOG_DIR}/pad.libre-service.eu/pad.libre-service.eu-access.log combined
        ErrorLog  ${APACHE_LOG_DIR}/pad.libre-service.eu/pad.libre-service.eu-error.log
        LogLevel warn

        SSLEngine On
        SSLCertificateFile /var/lib/dehydrated/certs/pad.libre-service.eu/fullchain.pem
        SSLCertificateKeyFile /var/lib/dehydrated/certs/pad.libre-service.eu/privkey.pem

        #Include hsts.conf
        #Include statoolinfos.conf
                                                                        
        DocumentRoot /var/www/pad.libre-service.eu
        #php_admin_value open_basedir "/var/www/pad.libre-service.eu/"
        <Directory "/var/www//pad.libre-service.eu/">
                Options FollowSymLinks
                AllowOverride None
                Require all granted
                DirectoryIndex index.xhtml
        </Directory>
</VirtualHost>
```

Activer le module SSL :   
```
apachectl configtest && systemctl reload apache2
```

Vérifier que c'est bon et recharger :
```
apachectl configtest && systemctl reload apache2
```


# Renouvellement automatique

Ajouter un script cron dans `/etc/cron.weekly/dehydrated` :
```
#!/bin/bash

/usr/bin/dehydrated -c
systemctl reload apache2.service
```

Et lui donner les bonnes permissions :
```
chmod a+rx /etc/cron.weekly/dehydrated
```
