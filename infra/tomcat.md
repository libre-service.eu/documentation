# Tomcat

Installer Tomcat :
```
apt-get install tomcat9
```

Installer le connecteur Apache2/Tomcat9 :
```
apt-get install libapache2-mod-jk
```

Configurer le connecteur :
```
TODO
```

Activer AJP dans Tomcat :
```
    <!-- Define an AJP 1.3 Connector on port 8009 -->
    <Connector protocol="AJP/1.3" address="::1" port="8009" redirectPort="8443" secretRequired="false" URIEncoding="UTF-8" />
```

Activer mod-jk dans la configuration web :
```                                                                     
        #
        JkMount /* ajp13_worker
</VirtualHost>

```

Dans les logs Tomcat `/var/log/tomcat9/catalina.out`, des alertes peuvent apparaître :
```
[2021-11-01 20:13:18] [warning] Incapable d'ajouter la ressource située [/WEB-INF/classes/] au cache de l'application web [] parce qu'il n'y avait pas assez d'espace libre disponible après l'éviction des entrées de cache expirées - envisagez d'augmenter la taille maximale du cache
[…]
[2021-11-01 20:13:18] [warning] Incapable d'ajouter la ressource située [/WEB-INF/classes/org/slf4j/impl/StaticLoggerBinder.class] au cache de l'application web [{1}] parce qu'il n'y avait pas assez d'espace libre disponible après l'éviction des entrées de cache expirées - envisagez d'augmenter la taille maximale du cache
```

Dans ce cas, configurer explicitement la taille du cache dans `/etc/tomcat9/context.xml` :
```
    <Resources cachingAllowed="true" cacheMaxSize="100000" />
</Context>
```
