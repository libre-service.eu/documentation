# Minetest

# Installation

Ouvrir le port par défaut : 
```
iptables -A INPUT  -i $WAN  -p udp  --dport 30000 -j ACCEPT
```

Installer le serveur Minetest :
```
apt-get install minetest-server minetestmapper
```

Pour privilégier le nommage des mondes, désactiver le monde par défaut :
```
systemctl stop minetest-server
systemctl disabled minetest-server
systemctl disabled minetest-server@minetest
```

Optimiser la configuration de Postgresql :
- avoir au moins 512MB de mémoire disponible pour Minetest hors Postgresql ;
- modifier `/etc/postgresql/13/main/postgresql.conf` :
```
-shared_buffers = 128MB                  # min 128kB
+shared_buffers = 512MB                  # min 128kB
```

# Mineclone2

## Install 
Récupérer le zip :
* https://content.minetest.net/packages/Wuzzy/mineclone2/
* cliquer sur le bouton `Download`

Décompresser :
```
unzip mineclone2_6745.zip
```

Ranger dans l'arborescence de Minetest :
```
mv mineclone2 /usr/share/games/minetest/games/
```

## Création d'un monde

Choisir un port et l'ouvrir : 
```
iptables -A INPUT  -i $WAN  -p udp  --dport 3000X -j ACCEPT
```

Créer un fichier de configuration élémentaire :
```
cp /etc/minetest/minetest.conf /etc/minetest/fooland.conf
```

Éditer le fichier de configuration `/etc/minetest/fooland.conf` : 
```
- server_name = My Minetest server
+ server_name = LibreServiceEU – Mineclone2 – Fooland

- server_description = Minetest server powered by Debian
+ server_description = Mineclone2 en libre-service sur LibreServiceEU

-# server_address = game.minetest.net
+ server_address = minetest.libre-service.eu

-# server_url = http://minetest.net
+ server_url = https://minetest.libre-service.eu/

- server_announce = false
+ server_announce = true

-port = 30000
+port = 3000X

-ipv6_server = false
+ipv6_server = true

- default_game = minetest
+ default_game = mineclone2

- motd = Welcome to Minetest. Enjoy your stay!
+ motd = Bienvenue sur le Minetest de Libre-service.eu \o/

-# map-dir =
+map-dir = /srv/minetest.libre-service.eu/worlds/fooland

- max_users = 15
+ max_users = 64

- enable_damage = false
+ enable_damage = true

- # disallow_empty_password = false
+ disallow_empty_password = true
```

Créer le dossier des mondes dans l'espace du service :
```
mkdir -p /srv/minetest.libre-service.eu/worlds
chown Debian-minetest /srv/minetest.libre-service.eu/worlds
```

Créer le nouveau monde :
```
systemctl start minetest-server@fooland.service
```

Le démarrer au prochain boot :
```
systemctl enable minetest-server@fooland.service
```

Tester le nouveau monde en se connectant avec un client Minetest.

## Migration dans Posgreql
Créer un compte dédié (si pas déjà fait) :
```
    su - postgres -c "psql -c \"create user minetestdba with password 'XXXXXXXX'; \" "
```

Créer une base de données :
```
su - postgres -c "psql -c \"CREATE DATABASE minetestdb_fooland OWNER minetestdba; \""
```

Associer le compte dédié avec la base fraîchement créée :
```
su - postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE minetestdb_fooland TO minetestdba; \""
```

Définir la base de données dans `/srv/minetest.libre-service.eu/worlds/fooland/world.mt` :
```
pgsql_connection = host=127.0.0.1 port=5432 user=minetestdba password=XXXXX dbname=minetestdb_fooland
pgsql_auth_connection = host=127.0.0.1 port=5432 user=minetestdba password=XXXXXX dbname=minetestdb_fooland
pgsql_player_connection = host=127.0.0.1 port=5432 user=minetestdba password=XXXXXX dbname=minetestdb_fooland
```

Migrer le nouveau monde vers le backend Postgres :
```
systemctl stop minetest-server@fooland.service
/usr/games/minetestserver --migrate postgresql --world /srv/minetest.libre-service.eu/worlds/fooland
/usr/games/minetestserver --migrate-auth postgresql --world /srv/minetest.libre-service.eu/worlds/fooland
/usr/games/minetestserver --migrate-players postgresql --world /srv/minetest.libre-service.eu/worlds/fooland
chown Debian-minetest.games /srv/minetest.libre-service.eu/worlds/fooland/world.mt
systemctl start minetest-server@fooland.service
```

## Module de protection de zone

Afin d'éviter le vandalisme, il est classique d'activer des fonctionnalités de protection de zone. Cela consiste à permettre au joueur de réserver une zone de blocs où il sera le seul à pouvoir casser/rajouter un bloc ou prendre/déposer dans un coffre.

Rappel : [documentation d'installation d'un mod](https://wiki.minetest.net/Installing_Mods). 

Le module [node_ownership](https://forum.minetest.net/viewtopic.php?t=846) est très connu mais obsolète.

Le module [areas](https://content.minetest.net/packages/ShadowNinja/areas/) semble à jour, maintenu et très utilisé ([voir graphique](https://wiki.minetest.net/Mods/fr)). Notamment, ce module est [utilisé sur Framinetest](https://framacloud.org/fr/cultiver-son-jardin/minetest.html).

Télécharger l'archive, puis dézipper et renommer : 
```
wget https://github.com/ShadowNinja/areas/archive/master.zip
unzip master.zip
mv areas-master areas
```

Déplacer dans le dossier du monde :
```
mkdir -p /srv/libreverse.chalec.org/worlds/fooland/worldmods
systemctl stop minetest-server@fooland.service
mv areas /srv/libreverse.chalec.org/worlds/libreverse/worldmods/
systemctl start minetest-server@fooland.service
```

Une aide est disponible en tapant `/help` dans le jeu.

Les réglages se font dans `/srv/minetest.libre-service.eu/worlds/fooland/worldmods/areas/settingtypes.txt`.

Les zones sont stockées dans `/srv/minetest.libre-service.eu/worlds/fooland/areas.dat` (c'est au format JSON).



## Jouer
Liens utiles :
- Mineclone2 : https://wiki.minetest.net/Games/MineClone_2/Differences_from_Minetest_Game
- Database backends : https://wiki.minetest.net/Database_backends
- Server commands : https://wiki.minetest.net/Server_commands
- Item strings : https://wiki.minetest.net/Itemstrings
- Privileges : https://wiki.minetest.net/Privilegesc

## Génération une carte du monde

### Migration du backend
La procédure utilise `minetestmapper` qui ne gère pas Postgresql, donc une étape va consister à convertir la base de donnée.

Copier temporairement la base :
```
su - postgres -c "psql -c \"CREATE DATABASE test with template minetestdb_libremine; \""
```

Copier temporairement l'arborescence du monde :
```
cp /srv/minetest.libre-service.eu/worlds/libremine /srv/minetest.libre-service.eu/worlds/test
```

Modifier le paramétrage de la base de données dans le fichier de paramétrage du monde :
```
sed -i 's/minetestdb_libremine/test/g' /srv/minetest.libre-service.eu/worlds/test/world.mt
```

Migrer le `backend` du monde (minetestmapper ne sais pas gérer postgresql):
```
/usr/games/minetestserver --migrate sqlite3 --world /srv/minetest.libre-service.eu/worlds/test
/usr/games/minetestserver --migrate-players sqlite3 --world /srv/minetest.libre-service.eu/worlds/test
```

### Correspondance des couleurs

Minetestmapper sait traduire les blocs Minetest en pixel de couleur mais Mineclone2 apporte de nouveaux blocs. La liste des correspondances de couleurs est donc à générer :
```
./mtsedit -m Mineclone2 -C > mineclone-colors.txt
```

### Génération
```
/usr/games/minetestmapper -i /srv/minetest.libre-service.eu/worlds/test/ -o /srv/minetest.libre-service.eu/foo.png --colors /srv/minetest.libre-service.eu/mineclone-colors.txt --draworigin --drawscale
```

### Nettoyage

Supprimer la base temporaire :
```
su - postgres -c "psql -c \"DROP DATABASE test; \""
```

Supprimer l'arborescence temporaire :
```
rm -fr /srv/minetest.libre-service.eu/worlds/test
```
