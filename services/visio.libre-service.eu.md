# Service visio.libre-service.eu
Le service visio.libre-service.eu est un service de vidéo-conférence basé sur le logiciel libre Jitsi : https://jitsi.org/

# Intallation

## Prérequis

Installer les paquets pré-requis :
```
apt-get install gnupg2 apt-transport-https openjdk-11-jre-headless

```

## Choix du serveur HTTP
Jitsi est prévu pour fonctionner avec Nginx ou Apache. Choix de Apache lors de l'installation du système Debian.

## Certificat web
Dans sa procédure d'installation, Jitsi laisse le choix soit de générer soi-même le certificat, soit de le faire automatiquement. Choix de gérer nous-même.
Installer les pré-requis :
```
apt-get install dehydrated dehydrated-apache2
systemctl reload apache2.service
/usr/bin/dehydrated --register --accept-terms
```
Ajouter `visio.libre-service.eu` dans `/etc/dehydrated/domains.txt`.

Lancer la génération :
```
/usr/bin/dehydrated -c
```

Activer le module SSL d'Apache :
```
a2enmod ssl
```

Activer un `virtualhost` pour le ssl, puis relancer Apache :
```
systemctl reload apache2.service
```

## Firewall

Ouverture de ports spécifiques :
```
iptables -A INPUT -i $WAN  -p udp  --dport 10000 -j ACCEPT
iptables -A INPUT -i $WAN  -p udp  --dport 3478 -j ACCEPT
iptables -A INPUT -i $WAN  -p tcp  --dport 5349 -j ACCEPT
```

## Hostname
Généraliser le FQDN :
```
hostnamectl set-hostname visio.libre-service.eu
```

Modifier le fichier `/etc/hosts` :
```
- 127.0.1.1       visio.libre-service.eu        visio
+ 127.0.1.1       visio
+ 145.239.49.4    visio.libre-service.eu
```

Tester :
```
ping "$(hostname)"
PING visio.libre-service.eu (145.239.49.4) 56(84) bytes of data.
64 bytes from visio.libre-service.eu (145.239.49.4): icmp_seq=1 ttl=64 time=0.034 ms
```

## Paquets Jitsi
Pour que le paquet Jitsi configure Apache, il faut neutraliser certains points :
```
a2dissite visio.libre-service.eu.conf
a2dismod ssl
systemctl reload apache2.service
cd /etc/apache2/sites-available/
mv visio.libre-service.eu.conf visio.libre-service.eu.conf.aco
```

Déclarer le dépôt Jitsi :
```
curl https://download.jitsi.org/jitsi-key.gpg.key | sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
```

Mettre à jour la liste des paquets :
```
apt update
```

Installer les paquets Jitsi :
```
apt install jitsi-meet
```

Répondre aux questions :
```
 Configuration de jitsi-videobridge2
 The value for the hostname that is set in Jitsi Videobridge installation.
 The hostname of the current installation:  visio.libre-service.eu

 SSL certificate for the Jitsi Meet instance
 Generate a new self-signed certificate (You will later get a chance to obtain a Let's encrypt certificate)
 I want to use my own certificate  <-- select this one

 Full local server path to the SSL key file:   /var/lib/dehydrated/certs/visio.libre-service.eu/privkey.pem

 Full local server path to the SSL certificate file:  /var/lib/dehydrated/certs/visio.libre-service.eu/fullchain.pem
```

Compléter le nouveau fichier de conf généré /etc/apache2/site-available/visio.libre-service.eu.conf` :
```
<VirtualHost *:80>
        ServerName visio.libre-service.eu
        ServerAdmin admins@libre-service.eu

        CustomLog ${APACHE_LOG_DIR}/visio.libre-service.eu-access.log combined
        ErrorLog  ${APACHE_LOG_DIR}/visio.libre-service.eu-error.log
        LogLevel warn

        Redirect 302 / https://visio.libre-service.eu/
</VirtualHost>

<VirtualHost *:443>
        ServerName visio.libre-service.eu
        ServerAdmin admins@libre-service.eu

        CustomLog ${APACHE_LOG_DIR}/visio.libre-service.eu-access.log combined
        ErrorLog  ${APACHE_LOG_DIR}/visio.libre-service.eu-error.log
        LogLevel warn

        # enable HTTP/2, if available
        Protocols h2 http/1.1

        SSLEngine on
        SSLProxyEngine on
        SSLCertificateFile /var/lib/dehydrated/certs/visio.libre-service.eu/fullchain.pem
        SSLCertificateKeyFile /var/lib/dehydrated/certs/visio.libre-service.eu/privkey.pem

        Header always set Strict-Transport-Security "max-age=63072000"

        DocumentRoot "/usr/share/jitsi-meet"
        <Directory "/usr/share/jitsi-meet">
                Options Indexes MultiViews Includes FollowSymLinks
                AddOutputFilter Includes html
                AllowOverride All
                Order allow,deny
                Allow from all
        </Directory>

        ErrorDocument 404 /static/404.html

        Alias "/config.js" "/etc/jitsi/meet/visio.libre-service.eu-config.js"
        <Location /config.js>
                Require all granted
        </Location>

        Alias "/external_api.js" "/usr/share/jitsi-meet/libs/external_api.min.js"
        <Location /external_api.js>
                Require all granted
        </Location>

        ProxyPreserveHost on
        ProxyPass /http-bind http://localhost:5280/http-bind
        ProxyPassReverse /http-bind http://localhost:5280/http-bind
        ProxyPass /xmpp-websocket ws://localhost:5280/xmpp-websocket
        ProxyPassReverse /xmpp-websocket ws://localhost:5280/xmpp-websocket
        ProxyPass /colibri-ws/default-id ws://localhost:9090/colibri-ws/default-id
        ProxyPassReverse /colibri-ws/default-id ws://localhost:9090/colibri-ws/default-id

        RewriteEngine on
        RewriteRule ^/([a-zA-Z0-9]+)$ /index.html
</VirtualHost>

# Mozilla Guideline v5.4, Apache 2.4.41, OpenSSL 1.1.1d, intermediate configuration, no OCSP
SSLProtocol         all -SSLv3 -TLSv1 -TLSv1.1
SSLCipherSuite      ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384
SSLHonorCipherOrder off
SSLSessionTickets   off
``` 

Tester l'installation.

## Limiter l'empreinte mémoire

Par défaut, la valeur maximale de mémoire allouable est 3072 Mo, ce qui peut paraître beaucoup.

Modifier le fichier `/usr/share/jitsi-videobridge/lib/videobridge.rc` :
```
VIDEOBRIDGE_MAX_MEMORY=2048m
```

## Activation des statistiques
Éditer le fichier `/etc/jitsi/videobridge/config` :

```
JVB_OPTS="--apis=rest, "
```

Redémarrer :
```
systemctl restart apache2.service  jicofo.service  jitsi-videobridge2.service
```

Tester :
```
root@visio.libre-service.eu:/etc# curl http://localhost:8080/about/version
{"name":"JVB","version":"2.1.570-gb802be83","os":"Linux"}

root@visio.libre-service.eu:/etc# curl http://localhost:8080/colibri/conferences
[]

root@visio.libre-service.eu:/etc# curl  http://localhost:8080/colibri/stats 
{"inactive_endpoints":0,"inactive_conferences":0,"total_ice_succeeded_relayed":0,…


root@visio.libre-service.eu:/etc# curl  http://localhost:8080/colibri/stats 2> /dev/null | jshon -e participants -u 9
0
```

Documentation :
* https://github.com/jitsi/jitsi-videobridge/blob/master/doc/rest.md 
* https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md#implementation 
* https://github.com/jitsi/jitsi-videobridge/blob/master/doc/rest-colibri.md

## Autoriser la connexion depuis un navigateur sur ordiphone
Éditer le fichier `/etc/jitsi/meet/visio.libre-service.eu-config.js` :

```
     // If true, any checks to handoff to another application will be prevented
     // and instead the app will continue to display in the current browser.
     // disableDeepLinking: false,
+    disableDeepLinking: true,
```

Redémarrer :
```
systemctl restart apache2.service  jicofo.service  jitsi-videobridge2.service
```

Tester depuis un ordiphone, par exemple depuis Firefox sur le Mobian d'un Pinephone :D


# Personnalisation

## Espace de fichiers

La personnalisation va passer par plusieurs fichiers à délivrer sur demande.

Créer un espace des fichiers de personnalisation web :
```
mkdir /var/www/visio.libre-service.eu/
mkdir /var/www/visio.libre-service.eu/images/
```

Copier le fichier de configuration de l'interface dans l'espace de fichiers personnalisés :
```
cp /usr/share/jitsi-meet/interface_config.js /var/www/visio.libre-service.eu/
```

Ajouter dans `/etc/apache2/sites-enabled/visio.libre-service.eu` (avant les autres location) :
```
   Alias "/interface_config.js" "/var/www/visio.libre-service.eu/interface_config.js"
```

Activer la nouvelle configuration :
```
systemctl reload apache2
```

## Parties tiers
Dans le fichier `/etc/jitsi/meet/visio.libre-service.eu-config.js`, désactiver les requêtes à des tiers (statistiques…) :
```
    disableThirdPartyRequests: true,
```

## Titres et onglet

Modifier le fichier `/var/www/visio.libre-service.eu/interface_config.js` :
```
<     APP_NAME: 'Jitsi Meet',
<     NATIVE_APP_NAME: 'Jitsi Meet',
<     PROVIDER_NAME: 'Jitsi',
---
>     APP_NAME: 'Visio.libre-service.eu',
>     NATIVE_APP_NAME: 'Visio.libre-service.eu',
>     PROVIDER_NAME: 'Libre-service.eu',
```


## Retrait pub privateur

Dans le fichier `/var/www/visio.libre-service.eu/interface_config.js` :
```
<     OPTIMAL_BROWSERS: [ 'chrome', 'chromium', 'firefox', 'nwjs', 'electron', 'safari' ],
---
>     OPTIMAL_BROWSERS: [ 'chromium', 'firefox', 'nwjs', 'electron' ],
```


## Alléger le menu

Retirer des entrées inutiles du menu, dans le fichier `/etc/jitsi/meet/` copier/décommenter la section `toolbarButtons` puis retirer les valeurs `livestreaming`, `linktosalesforce` et `recording`.

## Francisation

Franciser d'un pseudo par défaut dans `/var/www/visio.libre-service.eu/interface_config.js` :
```
<    DEFAULT_LOCAL_DISPLAY_NAME: 'me',
<    DEFAULT_REMOTE_DISPLAY_NAME: 'Fellow Jitster',
---
>    DEFAULT_LOCAL_DISPLAY_NAME: 'moi',
>    DEFAULT_REMOTE_DISPLAY_NAME: 'Inconnu',
```

## Watermark

Placer l'image au format SVG dans l'espace qui va bien : `/var/www/visio.libre-service.eu/images/`.

Ajouter dans `/etc/apache2/sites-enabled/visio.libre-service.eu` (avant les autres location) :
```
    Alias "/images/watermark.svg" "/var/www/visio.libre-service.eu/images/watermark.svg"
```

Modifier le fichier /var/www/visio.libre-service.eu/interface_config.js :
```
<     JITSI_WATERMARK_LINK: 'https://jitsi.org',
---
>     JITSI_WATERMARK_LINK: 'https://www.libre-service.eu/',
```

Activer :
```
systemctl restart apache2 jicofo.service  jitsi-videobridge2.service
```

## Favicon

Ajouter dans `/etc/apache2/sites-enabled/visio.libre-service.eu` (avant les autres location) :
```
   Alias "/images/favicon.ico" "/var/www/visio.libre-service.eu/images/favicon.ico"
```

Activer la nouvelle configuration :
```
systemctl reload apache2
```

## Pub privateur

Le pied de page de la page d'accueil contient des publicités vers des boutiques d'applications privatrices.

Supprimer le code suivant du fichier `/usr/share/jitsi-meet/libs/app.bundle.min.js`  :
```
C().createElement("a",{className:"welcome-badge",href:a},C().createElement("img",{alt:e("welcomepage.mobileDownLoadLinkIos"),src:"./images/app-store-badge.png"})),C().createElement("a",{className:"welcome-badge",href:t},C().createElement("img",{al\
t:e("welcomepage.mobileDownLoadLinkAndroid"),src:"./images/google-play-badge.png"})),
)),
```

Au besoin, regarder le fichier source `jitsi-meet/react/features/welcome/components/WelcomePage.web.js`.


Redémarrer les services Jitsi.
