# Service audio.libre-service.eu

Le service audio.libre-service.eu est un service d'audio-conférence basé sur le logiciel libre Mumble : https://www.mumble.info/

## Installation

Ouvrir les ports nécessaires :                              
```
iptables -A INPUT  -i $WAN  -p udp  --dport 64738 -j ACCEPT
iptables -A INPUT  -i $WAN  -p tcp  --dport 64738 -j ACCEPT
```

Installer le paquet :
```
apt-get install mumble-server
```

Configurer, notamment le mot de passe de SuperUser :
```
dpkg-reconfigure mumble-server
```

## Génération d'un certificat

Ajouter audio.libre-service.eu dans ``/etc/dehydrated/domains.txt``.

Lancer la prise en compte :
```
/usr/bin/dehydrated -c
```
Ajouter le chemin des certificats dans `/etc/mumble-server.ini` :
```
sslCert=/var/lib/dehydrated/certs/audio.libre-service.eu/fullchain.pem
sslKey=/var/lib/dehydrated/certs/audio.libre-service.eu/privkey.pem
```

Rendre les certificats lisibles par Mumble :
```
cd /var/lib/dehydrated/certs
chmod go+rX .
cd audio.libre-service.eu
chmod g+rX .
chown mumble-server . *
chmod g+r *
```

Redémarrer le service Mumble :
```
systemctl restart mumble-server
```

Ajouter les chmod et chgrp qui vont bien dans le script cron de Dehydrated.

## Amélioration de la qualité

Dans le fichier `/etc/mumble-server.ini` :
```
bandwidth=100000
```

## Modifier le message d'accueil à la connexion
Dans le fichier `/etc/mumble-server.ini` :
```
welcometext="<br />Bienvenue sur le service Mumble de <b>Libre-service.eu</b>.<br />https://www.libre-service.eu/<br />"
```

## Salons par défaut
Pour créer des salons par défaut, se connecter avec le compte `SuperUser` et faire clique-droit sur la racine puis `Ajouter`.

## Salons temporaires
Pour autoriser la création de salons temporaires :
- se connecter en tant que SuperUser
- racine > souris bouton droit > onglet `LCA` 
- cadre `LCAs Actifs`, sélectionner `@all` non italique
- cadre `Permissions` : cocher « Créer des temporaires »
- cliquer sur « OK ».

## Renommer la racine
Par défaut, la racine est nommée « Root ». Pour la renommer, éditer `/etc/mumble-server.ini` :
```                                                                           
registerName=Libre-service.eu
```
