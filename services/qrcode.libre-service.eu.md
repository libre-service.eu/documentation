# Service qrcode.libre-service.eu

Service de génération de code QR basé sur le logiciel libre Libreqr : https://code.antopie.org/miraty/libreqr/

# Installation

## Pré-requis

Installation des pré-requis :
```
apt install php7.4-gd imagemagick pngquant libapache2-mod-php7.4
```

## L'application

Choix de l'installation par le dépôt Git :
```
cd /var/www/
git clone https://code.antopie.org/miraty/libreqr.git qrcode.libre-service.eu
```

Sélection de la dernière branche officielle :
```
cd /var/www/qrcode.libre-service.eu/
git checkout -b 1.3.0
```

Créer une branche maison :
```
git checkout -b libre-service.eu-1.3.0
```

S'assurer des permissions :
```
chown www-data:www-data /var/www/qrcode.libre-service.eu/temp/
chmod ug+rwx /var/www/qrcode.libre-service.eu/temp/
```

## L'arborescence du service 

Tout service a son espace dédié :
```
mkdir -p /srv/qrcode.libre-service.eu/
cd /srv/qrcode.libre-service.eu/
ln -s /var/www/qrcode.libre-service.eu www
```

Création d'un dépôt `tools` à lier à un projet `qrcode.libre-service.eu-tools` sur la forge Devinsy :
```
cd /srv/qrcode.libre-service.eu/tools/
echo "# Service qrcode.libre-service.eu" > README.md
git remote add origin ssh://gitea@forge.libre-service.eu:/libre-service.eu/qrcode.libre-service.eu-tools.git
git push -u origin master
```

# Personnalisation

## Version

Veiller à mettre la bonne version dans le fichier `/var/www/qrcode.libre-service.eu/inc.php` :
```
-$libreqrVersion = "1.4.0dev";
+$libreqrVersion = "libre-service.eu-1.3.0";
```

## Pied de page (1/2)

Modifier le fichier `/var/www/qrcode.libre-service.eu/locales/en.php` :
```
-  'metaText_legal' => "LibreQR " . $libreqrVersion . " is a free software whose <a href='https://code.antopie.org/miraty/libreqr/'>source code</a> is available under the terms of the <abbr title='GNU Affero General Public License version 3 or any later version'><a href='LICENSE.html'>AGPLv3</a>+</abbr>.",
+  'metaText_legal' => "<a href='https://code.antopie.org/miraty/libreqr/'>LibreQR</a> version " . $libreqrVersion . " is a free software whose <a href='https://forge.devinsy.fr/libre-service.eu/qrcode.libre-service.eu-libreqr/'>source code</a> is available under the terms of the <abbr title='GNU Affero General Public License version 3 or any later version'><a href='LICENSE.html'>AGPLv3</a>+</abbr>.",
```

Modifier le fichier `/var/www/qrcode.libre-service.eu/locales/fr.php` :
```
-  'metaText_legal' => "LibreQR " . $libreqrVersion . " est un logiciel libre dont le <a href='https://code.antopie.org/miraty/libreqr/'>code source</a> est disponible selon les termes de l'<abbr title='GNU Affero General Public License version 3 ou toute version ultérieure'><a href='LICENSE.html'>AGPLv3</a>+</abbr>.",
+  'metaText_legal' => "<a href='https://code.antopie.org/miraty/libreqr/'>LibreQR</a> version" . $libreqrVersion . " est un logiciel libre dont le <a href='https://forge.devinsy.fr/libre-service.eu/qrcode.libre-service.eu-libreqr'>code source</a> est disponible selon les termes de l'<abbr title='GNU Affero General Public License version 3 ou toute version ultérieure'><a href='LICENSE.html'>AGPLv3</a>+</abbr>.",
```

## Pied de page (2/2)

Ajouter une phrase au pied de page dans le fichier `/var/www/qrcode.libre-service.eu/config.inc.php` :
```
-$customTextEnabled = false;
-$customText = "This LibreQR instance is hosted by <a href='https://foo.example/'>foo</a>.";
+$customTextEnabled = true;
+$customText = "Cette instance de LibreQR est hébergée  par <a href='https://www.libre-service.eu/'>https://www.libre-service.eu/</a>.";
```


## Thème personnalisé

Créer le thème personnalisé :
```
cd /var/www/qrcode.libre-service.eu/themes/
cp -a libreqr libre-service.eu
chmod -R go+rX libre-service.eu
```

Activer le thème personnalisé dans `config.inc.php` :
```
-$theme = "libreqr";
+$theme = "libre-service.eu";
```

Modifier le thème dans `themes/libre-service.eu/theme.php` :
```
-  "bg-light" => "white",
-  "bgField-light" => "#eeeeee",
+  "bg-light" => "#ff766c",
+  "bgField-light" => "#65c6e8",
```

## Favicon

Placer le fichier favicon source dans `themes/libre-service.eu/icons/source.png` puis :
```
cd /var/www/qrcode.libre-service.eu/
php themes/resize.php libre-service.eu
```

## Logo principal
Placer le fichier logo dans `themes/libre-service.eu/icons/libre-service.eu-libreqr-logo.png` puis modifier `index.php` :
```
-          <img alt="" id="logo" src="themes/<?php echo $theme; ?>/icons/128.png">
+          <img alt="" id="logo" src="themes/<?php echo $theme; ?>/icons/libre-service.eu-libreqr-logo.png">
```

## Titre
Modifier `index.php` :
```
-            <h1>LibreQR</h1>
+            <h1>QRCode.libre-service.eu</h1>
```


# Mise à jour
TODO