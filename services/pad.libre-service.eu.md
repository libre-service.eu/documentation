---
gitea: none
include_toc: true
---

# Service pad.libre-service.eu

Le service pad.libre-service.eu est un service de traitement de texte collaboratif basé sur le logiciel Etherpad-lite : https://etherpad.org/


# Installation 

Basée sur :
- https://github.com/ether/etherpad-lite#manual-install
- https://framacloud.org/fr/cultiver-son-jardin/etherpad


## Prérequis système

Configuration du hostname :
```
hostname pad.libre-service.eu
```

Personnalisation du prompt dans .bashrc :
```
PS1='${debian_chroot:+($debian_chroot)}\u@\H:\w\$ '
```
Ouvrir les ports nécessaires :                              
```
apt install ufw
ufw allow 'ssh'
ufw enable
```

## Prérequis logiciels

### Git 
Installation de Git pour cloner le code :  
```
apt install git
```

### Postgresql

Base de données :
```
apt install postgresql postgresql-contrib
```

D'après la documentation d'installation de Gitea :

>    PostgreSQL uses md5 challenge-response encryption scheme for password authentication by default.
>    Nowadays this scheme is not considered secure anymore. Use SCRAM-SHA-256 scheme instead by editing
>    the postgresql.conf configuration file on the database server to:
>    
>    password_encryption = scram-sha-256
>    
>    Restart PostgreSQL to apply the setting.

Le conseil semble judicieux donc éditer le fichier `/etc/postgresql/13/main/postgresql.conf` :

```
#password_encryption = md5              # md5 or scram-sha-256
password_encryption = scram-sha-256
```

Et redémarrer le service :

```
systemctl restart postgresql.service
```


### Nodejs

Etherpad nécessite Node.js >= 12.17.0. Vérifier que la version de Debian Stable est compatible :
```
apt-cache show nodejs |grep Version
```

Installer :
```
apt install nodejs
```

### Npm

Etherpad nécessite Npm. Installer `npm` :
```
apt install npm
```

## Déploiement

### Préparation de la base de données

Créer un utilisateur (« role » en langage Pgsql) :

```
   su -c "psql -c \"CREATE ROLE etherpad WITH LOGIN PASSWORD 'xxxxxxxxxxxxxxxxx';\"" - postgres
su -c "psql -c \"\du+\"" - postgres
```

Créer une base de données :
```
su -c "psql -c \"CREATE DATABASE etherpad WITH OWNER etherpad TEMPLATE template0 ENCODING UTF8 LC_COLLATE 'fr_FR.UTF-8' LC_CTYPE 'fr_FR.UTF-8';\"" - postgres
su -c "psql -c \"\l\"" - postgres
```

Donner l'accès en éditant le ficher `/etc/postgresql/13/main/pg_hba.conf` :
```
local    etherpad    etherpad    scram-sha-256
```

Redémarrer le service :
```
systemctl restart postgresql.service
```

### Création d'un utilisateur système dédié

Créer un compte système dédié :
```
adduser --disabled-password --system --group --home /srv/etherpad/ --shell /bin/bash etherpad
```

###  Déploiement du code

Déployer le code :
```
cd /var/www
git clone --branch master https://github.com/ether/etherpad-lite.git pad.libre-service.eu
chown -R etherpad:etherpad /var/www/pad.libre-service.eu
```

Créer le fichier de configuration :
```
su etherpad -c "cd /var/www/pad.libre-service.eu/ && cp settings.json.template settings.json"
```

Configurer dans `/var/www/pad.libre-service.eu/settings.json` :
```
- "dbType" : "dirty",
- "dbSettings" : {
-                   "filename" : "var/dirty.db"
-                 },

  "dbType" : "postgres",
  "dbSettings" : {
    "user":     "etherpad",
    "host":     "localhost",
    "port":     5432,
    "password": "password",
    "database": "etherpad",
    "charset":  "utf8mb4"
  },
[…]
  "trustProxy": true,
[…]
  "users": {
    "admin": {
      "password": "xxxxxxxxxxxxxxxx",
      "is_admin": true
    },
```

Premier lancement :
```
su etherpad -c "cd /var/www/pad.libre-service.eu/ && ./src/bin/run.sh"
```


## Configuration Systemd

Créer le fichier `/etc/systemd/system/etherpad.service` :
```
[Unit]
Description=Etherpad-lite, the collaborative editor.
After=syslog.target network.target postgresql.target

[Service]
Type=simple
User=etherpad
Group=etherpad
WorkingDirectory=/var/www/pad.libre-service.eu/
Environment=NODE_ENV=production

ExecStart=/var/www/pad.libre-service.eu/bin/fastRun.sh
StandardOutput=append:/var/log/etherpad/output.log
StandardError=append:/var/log/etherpad/error.log

Restart=always

[Install]
WantedBy=multi-user.target
```

Activer le fichier :
```
systemctl daemon-reload
```


Next, start the Etherpad service and enable it to start at system reboot with the following command:
```
systemctl start etherpad
systemctl status etherpad
```

Activer le démarrage automatique :
```
systemctl enable etherpad
```

## Configuration Apache

Documentation officielle : https://github.com/ether/etherpad-lite/wiki/How-to-put-Etherpad-Lite-behind-a-reverse-Proxy#apache

Activer les modules pour le proxy :
```
a2enmod proxy
a2enmod proxy_http
a2enmod rewrite
apachectl configtest
systemctl restart apache2
```

Configurer le site web :
```
        ProxyVia On
        ProxyRequests Off
        ProxyPreserveHost On

        <LocationMatch "^/(?!\.well-known/).*$">
                  ProxyPass http://localhost:9001 retry=0 timeout=30
                  ProxyPassReverse http://localhost:9001
        </LocationMatch>
        <Location /socket.io>
                  # This is needed to handle the websocket transport through the proxy, since                                                                                                                                                       
                  # etherpad does not use a specific sub-folder, such as /ws/ to handle this kind of traffic.                                                                                                                                       
                  # Taken from https://github.com/ether/etherpad-lite/issues/2318#issuecomment-63548542                                                                                                                                             
                  # Thanks to beaugunderson for the semantics                                                                                                                                                                                       
                  RewriteEngine On
                  RewriteCond %{QUERY_STRING} transport=websocket    [NC]
                  RewriteRule /(.*) ws://localhost:9001/socket.io/$1 [P,L]
                  ProxyPass http://localhost:9001/socket.io retry=0 timeout=30
                  ProxyPassReverse http://localhost:9001/socket.io
        </Location>

        <Proxy *>
               Options FollowSymLinks MultiViews
               AllowOverride All
               Order allow,deny
               allow from all
        </Proxy>
```


## Personnalisation

### Nom de l'instance

Modifier le nom de l'instance dans `/var/www/pad.libre-service.eu/settings.json` :
```
-  "title": "Etherpad",
+  "title": "pad.libre-service.eu",
```

### Favicon

Modifier le nom de l'instance dans `/var/www/pad.libre-service.eu/settings.json` :
```
-  "favicon": null,
+  "favicon": "/var/www/pad.libre-service.eu/custom/pad.libre-service.eu-logo.png",
```

### Texte de pad par défaut
Configurer un texte de pad par défaut dans `/var/www/pad.libre-service.eu/settings.json` :
```
-  "defaultPadText" : "Welcome to Etherpad!\n\nThis pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!\n\nGet involved with Etherpad at https:\/\/etherpad.org\n",

+   "defaultPadText" : "Bienvenue sur pad.libre-service.eu \o/\n\nLe service pad.libre-service.eu vous permet de faire de l'édition collaborative de texte.\nPour rappel, les pads non accédés depuis 1 an sont susceptibles d'être supprimés.\n\nEn vous souhaitant un agréable usage de nos services libres, éthique et loyaux.\n\nLibrement,\n\nL'équipe de Libre-service.eu. ",


```


### Installation de modules

Installer des modules élémentaires :
```
su - etherpad
cd /var/www/pad.libre-service.eu/
npm install --no-save --legacy-peer-deps ep_adminpads2 ep_align ep_author_hover ep_delete_empty_pads ep_font_color ep_font_size ep_headings2 ep_spellcheck ep_table_of_contents markdown
```

Sous réserve de mise à jour : ep_delete_after_delay.

Autres modules : ep_countable, ep_subscript_and_superscript.

### Ajout export ODF

La FAQ officielle indique comment faire : https://github.com/ether/etherpad-lite/wiki/How-to-enable-importing-and-exporting-different-file-formats-with-AbiWord

Installer LibreOffice de façon non grahique :
```
apt install libreoffice-nogui
```

Configurer dans `/var/www/pad.libre-service.eu/settings.json` : 
```
"soffice" : "/usr/bin/soffice"
```

### Augmentation de la taille d'import 
Lors de l'import d'un gros pad, les limites de téléversement peuvent bloquer l'import.

Augmenter les limites de téléversement dans `/var/www/pad.libre-service.eu/settings.json` : 
```
/* From Etherpad 1.8.3 onwards, the maximum allowed size for a single imported
 * file is always bounded.
 * File size is specified in bytes. Default is 50 MB.
 */
  "importMaxFileSize": 52428800, // 50 * 1024 * 1024
```

### Création d'un skin dédié
Une étape consiste à créer un skin dédié : 
```
su - etherpad
cd /var/www/pad.libre-service.eu/src/static/skins
cp -a colibris lseu
```

Et le déclaré dans le fichier de configuration `/var/www/pad.libre-service.eu/settings.json` :
```
-  "skinName": "colibris",
+  "skinName": "lseu",
```

### Table des matières non affichée sur petit écran
Sur les téléphones portables, l'affichage de la table des matières peut être inconfortable.

Une solution peut consister à ne pas afficher la table des matières par défaut. Ajouter dans `/var/www/pad.libre-service.eu/settings.json` :
```
"ep_toc": {
  "disable_by_default": true
}
```
Source : https://www.npmjs.com/package/ep_table_of_contents

La *solution retenue* consiste à automatiquement ne pas afficher la table des matières si l'écran n'est pas assez large. Ajouter dans `/var/www/pad.libre-service.eu/src/static/skins/lseu/pad.css` :
```
@media (max-width: 590px) {
    #toc { display: none !important;}
}
```

### Table des matières trop large

Par défaut, l'affichage de la table des matières déplace la zone d'édition vers la droite et la place contre la colonne de droite rendant difficile la lecture de la page.

Une solution consite à réduire la taille de la table des matières. Modifier le fichier `/var/www/pad.libre-service.eu/node_modules/ep_table_of_contents/static/css/toc.css` :
```
@media (min-width: 1350px) {
-  #toc { width: 25% }
+  #toc { width: 20% }
  body.comments-active #toc { width: 20% } // a bit smaller when the comment sidebar is visible
}
```

### Table des matière avec liens soulignés

Par défaut, dans la table des matières, les liens sont soulignés. Modifier `/var/www/pad.libre-service.eu/node_modules/ep_table_of_contents/static/css/toc.css` :
```
.tocItem{
  white-space: nowrap;
  text-overflow: ellipsis;
  display: block;
  overflow: hidden;
  cursor:pointer;
+ text-decoration: none;
}

```

### Couleurs de fond

Par défaut, les couleur de fond de la table des matières et de la zone d'édition sont gris, très triste., les liens sont soulignés.

Pour le fond de la table des matières, modifier `/var/www/pad.libre-service.eu/src/static/skins/lseu/src/components/table-of-content.css` :
```
#toc {
  padding: 20px 20px 10px 10px !important;
  min-width: 146px !important;
- background-color: transparent !important;
+ background-color: #edf2f7 !important;
+ color: #2c6a82 !important;
  border: none !important;
  order: -2;
}
```

Pour le fond de la zone d'édition, modifier `/var/www/pad.libre-service.eu/src/static/skins/lseu/src/pad-variants.css` :
```
/* ====================== */
/* == Light Background == */
/* ====================== */
.light-background #editorcontainerbox, .light-background #sidediv,                                                                                                        
.light-background #chatbox, .light-background #outerdocbody, .light-background {
  --text-color: var(--super-dark-color);
  --text-soft-color: var(--dark-color);
  --border-color: var(--middle-color);
  --bg-soft-color: var(--super-light-color);
- --bg-color: var(--light-color);
+ --bg-color: #e2e8f0;
}
```


# Mise à jour
Documentation:
```
To update to the latest released version, execute git pull origin. The next start with src/bin/run.sh will update the dependencies.
```

So proceed: 
```
systemctl stop etherpad
su etherpad -c "cd /var/www/pad.libre-service.eu/ && git pull"
su etherpad -c "cd /var/www/pad.libre-service.eu/ && ./bin/installDeps.sh"

systemctl start etherpad
```

Update all plugins **WORK IN PROGRESS**:
```
systemctl stop etherpad
su etherpad -c "cd /var/www/pad.libre-service.eu/ && npm update -g"

systemctl start etherpad
```
