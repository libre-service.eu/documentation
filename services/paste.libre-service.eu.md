# Service paste.libre-service.eu

Le service paste.libre-service.eu est un service de « paste » basé sur le logiciel libre Privatebin : https://privatebin.info.

# Installation

Les données peuvent être stockées en SQL ou en fichier plat. Ici la solution retenue est fichiers plats.

## Prérequis

S'assurer que certains paquets soient installés :
```
apt-get install php7.4-gd
```

## Déploiement du code
Choix de l'installation par le dépôt Git :
```
cd /var/www/
git clone https://github.com/PrivateBin/PrivateBin paste.libre-service.eu
```

Créer une branche maison à partir de la dernière branche officielle :
```
cd /var/www/paste.libre-service.eu/
git checkout -b libre-service.eu-1.3.5 1.3.5
```

Créer le dossier des données :
```
mkdir /var/www/paste.libre-service.eu/data
chown www-data.www-data /var/www/paste.libre-service.eu/data
```

## Fichiers de logs
Suivre la convention de créer un dossier spécifique au log Apache du service :
```
mkdir -p /var/log/apache2/paste.libre-service.eu
```

# Dépôts
Dépôts maison :
* https://forge.devinsy.fr/libre-service.eu/paste.libre-service.eu-pastebin
* https://forge.devinsy.fr/libre-service.eu/paste.libre-service.eu-tools


## Configuration d'Apache
Suivre les conventions d'architecture.

# Personnalisation

## Nom du service
Modifier le fichier `/var/www/paste.libre-service.eu/cfg/conf.php` :
```
-; name = "PrivateBin"  
+name = "Paste.libre-service.eu"
```

## Lien vers les sources
Modifier le fichier `/var/www/paste.libre-service.eu/tpl/bootstrap.php` :
```
-                                       <p class="col-md-1 col-xs-4 text-center"><?php echo $VERSION; ?></p>
+                                       <p class="col-md-1 col-xs-4 text-center"><a href="https://forge.devinsy.fr/libre-service.eu/paste.libre-service.eu-pastebin"><?php echo "version libre-service.eu-$VERSION"; ?></a></p>
```

# Patch pour les métriques
Par défaut, PrivateBin ne permet pas de comptabiliser la création ou la destruction d'un pad.

Un patch d'une quinzaine de lignes est nécessaire : https://forge.devinsy.fr/libre-service.eu/paste.libre-service.eu-pastebin/commit/f70ef782294ac0b9d380c04157f1d7b2117c6b6b